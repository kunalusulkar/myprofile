import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-work',
  templateUrl: 'work.html',
})
export class WorkPage {

  public info = {
    "fullname": "Kunal Usulkar",
    "dob": "12 JUL 1990",
    "ic": "",
    "gender": "Male",
    "race": "Maratha",
    "religion": "Hindu",
    "nationality": "Indian",
    "status": "Single",
    "language": "English, Marathi, Hindi",
    "email": "usulkarkunal@gmail.com",
    "phone": "0564499048",
    "address": {
      "street": "Al Twash Building, Bur Dubai",
      "street2": "Dubai"
    },

    
    "experience": [{
      "company": "Etisalat Telecommunications",
      "aboutCompany": "Etisalat is a leading multinational Emirati based telecommunications services provider, currently operating in 16 countries across Asia, the Middle East and Africa.",    
      "job": "Full Stack Developer",
      "date": "Dec 2017 - till Date",
      "place": "Dubai",
      "responsibility": [
        "Developing and handling multiple internal project for etisalat(HR and Sales application)",
        "Involed in both Mobile and Web application development",
        "Major projects worked on HRConnect, Sales POS Automation and Sales Frontline Automation",
        "Technologies : Java, Oracle Sql, Angular, Ionic, Spring, Hibernate, Weblogic."
      ],
      "img": "assets/etisalat.png"
    },
    {
      "company": "McLaren Software",
      "aboutCompany": "Opidis (formerly McLaren Software) is a Product company which delivers engineering document management solutions for Owner Operators and EPCs in asset-intensive industries.",    
      "job": "Web and Java Developer",
      "date": "Aug 2016 - Dec 2017",
      "place": "Pune",
      "responsibility": [
        "McLaren FL is a cloud-based solution which improves supports and simplifies project collaboration and document management for construction and engineering projects of all sizes.",
        "Involed in Developing different modules of Product like Approval cycle, Subscription, CAD Integration, Permissions rights for different features of Product.",
        "Involed in requirement gathering and conducting retrospective meeting",
        "Client	: GP Papenburg Hochbau GmbH, Synerail, PM Group, Aspire, Strabag, MAN Diesel SE and Warbud",
        "Technologies : Java 1.6, JSP 2.0, Spring core,  Hibernate 3.6, Oracle SQL server, Tomcat, Angular JS, EXTJS 3, jQuery."
      ],
      "img": "assets/idox.png"
    },
    {
      "company": "Tata Consultancy Services",
      "aboutCompany": "Tata Consultancy Services is a global leader in IT services, consulting & business solutions with a large network of innovation & delivery centers",
      "job": "Programmer",
      "date": "2012-2016",
      "place": "Pune",
      "responsibility": [
        "Joined as Developer role, Worked on multiple project through out my experience with TCS",
        "Involed in developing Accept Wizard Application on top of Accept 360 database - Product data management (PDM) for Nokia and Microsoft Client.",
        "Developed Sharepoint application using jquery SP Services to fetch data from sharepoint.",
        "Technologies : Java, Jquery, Underscore,js, SlickGrid.js, HTML, CSS, Maven, Oracle 10g , tortoise SVN, Putty, WinScp, Siemens Teamcenter"

      ],
      "img": "assets/tcs.jpg"
    }
    ]
    
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //this.info = this.navParams.get('info');
  }



}
