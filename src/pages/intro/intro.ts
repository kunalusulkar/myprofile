import { AdMobFreeBannerConfig, AdMobFree } from '@ionic-native/admob-free';
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {


  // public info = new Array();
  public info = {
    "fullname": "Kunal Usulkar",
    "dob": "12 JUL 1990",
    "ic": "",
    "gender": "Male",
    "race": "Maratha",
    "religion": "Hindu",
    "nationality": "Indian",
    "status": "Single",
    "language": "English, Marathi, Hindi",
    "email": "usulkarkunal@gmail.com",
    "phone": "+971-564499048",
    "location" : "Dubai - United Arab Emirates",
    "address": {
      "street": "Al Twash Building, Bur Dubai",
      "street2": "Dubai"
    },

    "education": {
      // "Bachelor": {
      //   "title": "Bachelor in Electronics and Communication",
      //   "place": "Universiti Putra Malaysia (UPM)",
      //   "major": "Software Engineering",
      //   "year": "2018",
      //   "cgpa": "3.xx"
      // },
      "Bachelor": {
        "title": "Bachelor in Electronics and Communication",
        "college": "KLES College of Engineering",
        "place": "Belgaum",
        "year": "2012",
        "percentage": "79%"
      },
      "puc": {
        "title": "PUC 2nd Yr (12th)",
        "college": "GSS College of Science",
        "place": "Belgaum",
        "year": "2008",
        "percentage": "80%"
      },
      "school": {
        "title": "Schooling",
        "college": "St Mary's High School",
        "place": "Belgaum",
        "year": "2006",
        "percentage": "84%"
      }
    },
    "seminar": [{
      "title": "Mobile App Development Crash Course (Phonegap)",
      "conduct": "Conducted by Alumni of Computer Science UPM",
      "date": "26 November 2016",
      "img": "assets/phonegap.jpg"
    },
    {
      "title": "AWS Internet of Things Course (Hilti IOT Competition Program 2017)",
      "conduct": "Conducted by Hilti Instructor",
      "date": "19 November 2016",
      "img": "assets/hilti.jpeg"
    },
    {
      "title": "Joomla 3.0 Crash Course",
      "conduct": "Conducted by Caspian Technology.",
      "date": "23 January 2016",
      "img": "assets/joomla.jpg"
    },
    {
      "title": "Bootstrap Studio Training",
      "conduct": "Conducted by Nasrul Hazim from Cleanique Coders Resources",
      "date": "16 Januari 2016",
      "img": "assets/bootstrap.jpg"
    },
    {
      "title": "Certified in Industrial Level Java Web-Based Application",
      "conduct": "Conducted by MIMOS BHD",
      "date": "28 Oct - 29 Nov 2013",
      "img": "assets/mimos.jpeg"
    },
    {
      "title": "ComTIA A+ Certified CE.",
      "conduct": "Conducted by Prestariang",
      "date": "7 June 2013",
      "img": "assets/comptia.jpg"
    }
    ],
    "knowledge": {
      "computer": "Java Programming, Web Programming (HTML5, CSS, Javascript, JQuery, JSP, SQL). Familiar with framework like Ionic Framework, Angular, UnderscoreJs, Spring , Hibernate, Struts, Oracle Database, ",
      "personal": "Love to take challenges, cool minded, team player and multitasker, highly motivated, commited to perfection, hardworking and can work in team."
    },
    "achievement": [
      "Etisalat - 2 times OPCA Award, Monthly Excellence Award",
      "Mclaren Softwares - Customer Satisfaction Award - for CAD Integeration module developed",
      "TCS - Awards for Excellence (On The Spot Award)  - For successfully improving the quality of Application.",
      "TCS - Star of Month Award - For successfully delivering project on time."
    ],
    "interest": [
      "Exploring new programming languages such as Angular, React Native, Ionic, Docker, Solr, Elastic search engine and many latest technology trends",
      "Following the latest trends and eager to participate in any continuing education opportunities that are available."
      
    ],
    "references": [{
      "name": "Sameer Shaikh",
      "title": "Director - Mclaren Softwares",
      "email": "Sameer.shaikh@idoxgroup.com"
    },
    {
      "name": "Rohit Sarpotdar",
      "title": "Assistant Consultant",
      "email": "rohitsarpotdar28@gmail.com"
    }
    ]
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public adMobFree:AdMobFree) {
    // let localData = http.get('assets/resume.json')
    //   .map(res => res.json());
    // // .map(res => res.details);
    // localData.subscribe(data => {
    //   console.log(data.fullname);
    //   this.info = data;

    //   console.log(this.info);
    // })
    // let status bar overlay webview
    this.showBannerAd();
  }
  //ads
  async showBannerAd() {
    try {
      const bannerConfig: AdMobFreeBannerConfig = {
        id:'ca-app-pub-8469816531943468/3705609592',
        isTesting: false,
        autoShow: true
      }

      this.adMobFree.banner.config(bannerConfig);

      const result = await this.adMobFree.banner.prepare();
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

//reset splash je, testing je
  resetStorage() {
    this.storage.clear();
    // this.navCtrl.setRoot(SplashPage)
  }

  //navigate to SeminarPage
  goSeminar(info) {
    this.navCtrl.push('SeminarPage', {
      'info': this.info
    });
  }

  //navigate to WorkPage
  goWork(info) {
    this.navCtrl.push('WorkPage', {
      'info': this.info
    });
  }
}